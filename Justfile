#!/bin/just -f

build package:
  cd 'packages/{{package}}' && pnpm run build

docs package:
  cd 'packages/{{package}}' && pnpm run docs

docs__publish package: (docs package)
  #!/bin/sh -e
  git branch -D pages || true
  cp -r 'packages/{{package}}/pages' '/tmp/{{package}}-pages'
  cd '/tmp/{{package}}-pages'
  git init
  git switch --orphan pages
  git add .
  git commit -m 'Deploy docs'
  git remote add origin git@codeberg.org:libreedu/components.git
  git push -f -u origin pages

prettify package='*':
  bash -c 'deno fmt packages/{{package}}/{src,tests,*.js}'
