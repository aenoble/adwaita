/**
 * @file
 *
 * This script takes `src` directory and makes a tsc/typedoc-compatible
 * copy in `node` directory, by sloppily replacing import paths.
 */

const { join } = require('node:path');
const {
  existsSync,
  mkdirSync,
  readdirSync,
  readFileSync,
  statSync,
  writeFileSync,
} = require('node:fs');

function find(dirname, directories) {
  const files = [];
  readdirSync(dirname).forEach((f) => {
    const path = join(dirname, f);
    if (statSync(path).isDirectory()) {
      if (directories) files.push(path);
      files.push(...find(path, directories));
    } else if (!directories) files.push(path);
  });
  return files;
}

if (!existsSync('node')) mkdirSync('node');

find('src', true).forEach((d) => {
  const outDir = 'node/' + d.slice('src/'.length);
  if (!existsSync(outDir)) mkdirSync(outDir);
});

find('src').forEach((f) => {
  const content = readFileSync(f, 'utf8');
  const outFile = 'node/' + f.slice('src/'.length);
  const lines = content.split('\n').map((l) => {
    switch (l.slice(0, 6)) {
      case 'export':
      case 'import':
      case '} from':
        return l.replace(/\.ts/g, '');
      default:
        return l;
    }
  });
  writeFileSync(outFile, lines.join('\n'));
});
