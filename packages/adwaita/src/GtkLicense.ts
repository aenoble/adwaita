/** @namespace  */
enum GtkLicense {
  /** No license specified. */
  UNKNOWN,
  /** A license text is going to be specified by the developer. */
  CUSTOM,
  /** The GNU General Public License, version 2.0 or later. */
  GPL_2_0,
  /** The GNU General Public License, version 3.0 or later. */
  GPL_3_0,
  /** The GNU Lesser General Public License, version 2.1 or later. */
  LGPL_2_1,
  /** The GNU Lesser General Public License, version 3.0 or later. */
  LGPL_3_0,
  /** The BSD standard license. */
  BSD,
  /** The MIT/X11 standard license. */
  MIT_X11,
  /** The Artistic License, version 2.0 */
  ARTISTIC,
  /** The GNU General Public License, version 2.0 only. */
  GPL_2_0_ONLY,
  /** The GNU General Public License, version 3.0 only. */
  GPL_3_0_ONLY,
  /** The GNU Lesser General Public License, version 2.1 only. */
  LGPL_2_1_ONLY,
  /** The GNU Lesser General Public License, version 3.0 only. */
  LGPL_3_0_ONLY,
  /** The GNU Affero General Public License, version 3.0 or later. */
  AGPL_3_0,
  /** The GNU Affero General Public License, version 3.0 only. */
  AGPL_3_0_ONLY,
  /** The 3-clause BSD license. */
  BSD_3,
  /** The Apache License, version 2.0 */
  APACHE_2_0,
  /** The Mozilla Public License, version 2.0 */
  MPL_2_0,
}

export default GtkLicense;
