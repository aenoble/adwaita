/**
 * Control how a widget deals with extra space in a single dimension.
 *
 * Alignment only matters if the widget receives a "too large"
 * allocation, for example if you packed the widget with the
 * {@link GtkWidget.hexpand} property inside a {@link GtkBox}, then the
 * widget may get extra space. If you have for example a 16x16 icon
 * inside a 32x32 space, the icon could be scaled and stretched, it
 * could be centered, or it could be positioned to one side of the
 * space.
 *
 * Note that in horizontal context {@link GtkAlign.START} and
 * {@link GtkAlign.END} are interpreted relative to text direction.
 *
 * {@link GtkAlign.BASELINE} support is optional for containers and
 * widgets, and it is only supported for vertical alignment. When it's
 * not supported by a child or a container it is treated as
 * {@link GtkAlign.FILL}.
 */
enum GtkAlign {
  /**
   * Stretch to fill all space if possible, center if no meaningful
   * way to stretch.
   */
  FILL,
  /** Snap to left or top side, leaving space on right or bottom. */
  START,
  /** Snap to right or bottom side, leaving space on left or top. */
  END,
  /** Center natural width of widget inside the allocation. */
  CENTER,
  /**
   * Align the widget according to the baseline.
   *
   * @see {@link GtkWidget}
   */
  BASELINE,
}

export default GtkAlign;
