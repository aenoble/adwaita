/**
 * Describes whether a {@link GtkFileChooser} is being used to open
 * existing files or to save to a possibly new file.
 *
 * @unstable GtkFileChooser is unimplemented.
 */
enum GtkFileChooserAction {
  /**
   * Indicates open mode. The file chooser will let the user pick an
   * existing file.
   */
  OPEN,
  /**
   * Indicates save mode. The file chooser will let the user pick an
   * existing file, or type in a new filename.
   */
  SAVE,
  /**
   * Indicates an Open mode for selecting folders. The file chooser will
   * let the user pick an existing folder.
   */
  SELECT_FOLDER,
}

export default GtkFileChooserAction;
