enum GtkBaselinePosition {
  /** Align the baseline at the top. */
  TOP,
  /** Center the baseline. */
  CENTER,
  /** Align the baseline at the bottom. */
  BOTTOM,
}

export default GtkBaselinePosition;
