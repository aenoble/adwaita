import GtkAlign from './GtkAlign.ts';
import GtkOverflow from './GtkOverflow.ts';

/**
 * `GtkWidget` is the base class all widgets in GTK derive from. It
 * manages the widget lifecycle, layout, states and style.
 *
 * Subclasses must have the {@link GtkWidget.realize} method.
 *
 * If a widget has children, it must call {@link GtkWidget.realize} for
 * each of them upon rendering.
 *
 * @unstable
 * Focus and pointer-target may not be possible to implement.
 *
 * @todo Signals -- GSignal is currently unimplemented.
 *
 * Implement GtkAccessible, GtkBuildable, GtkConstraintTarget.
 */
abstract class GtkWidget {
  /**
   * Whether the widget or any of its descendents can accept the input
   * focus.
   *
   * This property is meant to be set by widget implementations,
   * typically in their instance init function.
   */
  public canFocus: boolean | void = void 0;

  /** Whether the widget can receive pointer events. */
  public canTarget: boolean | void = void 0;

  /** A list of CSS classes applied to this widget. */
  public cssClasses: string[] = [];

  /**
   * Whether the widget should grab focus when it is clicked with the
   * mouse.
   *
   * This property is only relevant for widgets that can take focus.
   *
   * @todo *
   */
  public focusOnClick = false;

  /** Whether this widget itself will accept the input focus. @todo * */
  public focusable = false;

  /** How to distribute horizontal space if widget gets extra space. */
  public halign: GtkAlign = GtkAlign.CENTER;

  /** Whether the widget is the default widget. @todo * */
  public readonly hasDefault = false;

  /** Whether the widget has input focus. @todo * */
  public readonly hasFocus = false;

  /**
   * Enables or disables the emission of the
   * {@link GtkWidget.queryTooltip} signal on the widget.
   *
   * A value of `true` indicates that the widget can have a tooltip, in
   * this case the widget will be queried using
   * {@link GtkWidget.queryTooltip} to determine whether it will provide
   * a tooltip or not.
   *
   * @todo *
   */
  public hasTooltip = false;

  /**
   * Override for the height request of the widget.
   *
   * If this is -1, the natural request will be used.
   *
   * @unstable
   */
  public heightRequest = -1;

  /**
   * Whether to expand horizontally.
   *
   * @todo *
   */
  public hexpand = false;

  /** Whether to use the `hexpand` property. @deprecated */
  public hexpandSet = true;

  /** @unstable GtkLayoutManager is unimplemented. */
  public layoutManager: void = void 0; // FIXME: Type

  /** Margin on the bottom side of the widget. */
  public marginBottom = 0;

  /**
   * Margin on end of the widget, horizontally.
   *
   * This property supports left-to-right and right-to-left text
   * directions.
   */
  public marginEnd = 0;

  /** Margin on start of the widget, horizontally.
   *
   * This property supports left-to-right and right-to-left text
   * directions.
   */
  public marginStart = 0;

  /** Margin on top side of the widget. */
  public marginTop = 0;

  /** The name of the widget. */
  public abstract name: string;

  /** The requested opacity of the widget. */
  public opacity = 1;

  /**
   * How content outside the widget's content area is treated.
   *
   * This property is meant to be set my widget implementations,
   * typically in their instance init function.
   */
  public overflow: GtkOverflow = GtkOverflow.VISIBLE;

  /** The parent widget of this widget. */
  public readonly parent: GtkWidget | undefined;

  /**
   * Whether the widget will receive the default action when it is
   * focused.
   *
   * @todo *
   *
   * @unstable
   */
  public receivesDefault = false;

  /**
   * The {@link GtkRoot} widget of the widget tree containing this
   * widget.
   *
   * This will be `null` if the widget is not contained in a root
   * widget.
   */
  public abstract readonly root: void; // FIXME: Type

  /** The scale factor of the widget. */
  public scaleFactor = 1;

  /** Whether the widget responds to input. @unstable */
  public sensitive = false;

  /**
   * Sets the text of tooltip to be the given string, which is marked up
   * with Pango markup.
   *
   * Also see {@link GtkTooltip.setMarkup}.
   *
   * This is a convenience property which will take care of getting the
   * tooltip shown if the given string is not `null`:
   * {@link GtkWidget.hasTooltip} will automatically be set to `true`
   * and there will be taken care of {@link GtkWidget.queryTooltip} in
   * the default signal handler.
   *
   * Note that if both {@link GtkWidget.tooltipText} and
   * {@link GtkWidget.tooltipMarkup} are set, the last one wins.
   *
   * @unstable GtkTooltip is unimplemented. Format will likely change.
   * Also, "the last one wins" is ambiguous and behavior is undefined.
   *
   * @todo Accessors
   *
   * @see {@link GtkTooltip.setMarkup}
   */
  public tooltipMarkup = '';

  /**
   * Sets the text of tooltip to be the given string.
   *
   * Also see {@link GtkTooltip.setText}.
   *
   * This is a convenience property which will take care of getting the
   * tooltip shown if the given string is not `null`:
   * {@link GtkWidget.hasTooltip} will automatically be set to `true`
   * and there will be taken care of {@link GtkWidget.queryTooltip} in
   * the defaualt signal handler.
   *
   * Note that if both {@link GtkWidget.tooltipText} and
   * {@link GtkWidget.tooltipMarkup} are set, the last one wins.
   *
   * @unstable GtkTooltip is unimplemented. Format will likely change.
   * Also, "the last one wins" is ambiguous and behavior is undefined.
   *
   * @todo Accessors
   *
   * @see {@link GtkTooltip.setText}
   */
  public tooltipText = '';

  /** How to distribute vertical space if widget gets extra space. */
  public valign: GtkAlign = GtkAlign.CENTER;

  /**
   * Whether to expand vertically.
   *
   * @todo *
   */
  public vexpand = false;

  /** Whether to use the `vexpand` property. @deprecated */
  public vexpandSet = true;

  /**
   * Whether the widget is visible.
   *
   * @todo Accessors
   */
  public visible = false;

  /**
   * Override for the width request of the widget.
   *
   * If this is -1, the natural request will be used.
   *
   * @unstable
   */
  public widthRequest = -1;

  /** Renders the component.
   *
   * @todo HTMLElement should probably be stored somewhere, so
   * properties like {@link GtkWidget.visible} can be modified after
   * this is called.
   */
  abstract realize(): HTMLElement;
  /**
   * Calls {@link GtkWidget.realize} and replaces children of `frame`
   * with the rendered component.
   *
   * @param {Node} frame The container for the widget
   *
   * @unstable This may be removed.
   */
  public present(frame: ParentNode) {
    frame.replaceChildren(this.realize());
  }
}

export default GtkWidget;
