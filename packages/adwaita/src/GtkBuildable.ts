/**
 * @unstable
 * Unimplemented. The end implementation may simply be JSX support
 * within the code.
 *
 * @todo --
 */
// deno-lint-ignore no-empty-interface
interface GtkBuildable {}

export default GtkBuildable;
