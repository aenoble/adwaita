/**
 * Used for justifying the text inside a {@link GtkLabel} widget.
 *
 * @unstable GtkLabel is unimplemented.
 */
enum GtkJustification {
  /** The text is placed at the left edge of the label. */
  LEFT,
  /** The text is placed at the right edge of the label. */
  RIGHT,
  /** The text is placed at the center of the label. */
  CENTER,
  /** The text is placed is distributed across the label. */
  FILL,
}

export default GtkJustification;
