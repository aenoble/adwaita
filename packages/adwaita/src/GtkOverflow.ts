/**
 * Defines how content overflowing a given area should be handled.
 *
 * This is used in {@link GtkWidget.overflow}. The
 * {@link GtkWidget.overflow} property is modeled after the CSS overflow
 * property, but implements it only partially.
 */
enum GtkOverflow {
  /**
   * No change is aplied. Content is drawn at the specified position.
   */
  VISIBLE,
  /**
   * Content is clipped to the bounds of the area. Content outside the
   * area is not drawn and cannot be interfaced with.
   */
  HIDDEN,
}

export default GtkOverflow;
