import DchDebounceSignal from './DchDebounceSignal.ts';
import GSignal from './GSignal.ts';

/**
 * A selection model is a simple interface to add support for selections
 * to a list model.
 *
 * Adwaita provides the most common selection modes like GTK; see, e.g.,
 * {@link DchMultiSelection}.
 *
 * GtkSelectionModel looked overcomplicated at first glance, so this
 * exists now.
 *
 * @see {@link DchMultiSelection}
 * @see {@link DchNoSelection}
 * @see {@link DchSingleSelection}
 *
 * @typeParam I What type the identifiers are.
 */
export abstract class DchSelectionModel<
  I extends string | number | symbol,
> {
  public model: Record<I, boolean>;

  /**
   * @param s The item to deselect
   * @returns Whether the item was selected
   * @emits {@link DchSelectionModel.selectionChanged}
   */
  public abstract select(s: I): boolean;

  /**
   * @param s The item to deselect
   * @returns Whether the item was deselected
   * @emits {@link DchSelectionModel.selectionChanged}
   */
  public abstract deselect(s: I): boolean;

  /** @returns Whether _all_ items were successfully selected */
  public selectAll(): boolean {
    this.getSelection().forEach(this.deselect);
    return Object.keys(this.model).length == this.getSelection().length;
  }

  /** @returns Whether _all_ items were successfully deselected */
  public deselectAll(): boolean {
    this.getSelection().forEach(this.deselect);
    return this.getSelection().length == 0;
  }

  /** @param s @returns Whether `s` is selected */
  public isSelected(s: I): boolean {
    return this.model[s];
  }

  public getSelection(): I[] {
    const i: I[] = [];
    for (const n in this.model) {
      if (this.isSelected(n)) i.push(n);
    }

    return i;
  }

  /**
   * Emitted when the selection state of some of the items in
   * {@link DchSelectionModel.model} changes.
   *
   * Debounces for 10ms.
   */
  public selectionChanged: GSignal<[]> = new DchDebounceSignal<[]>(10);

  /** @param m The model to use. If not provided, `{}` is used. */
  public constructor(m: Record<I, boolean>) {
    this.model = m || <Record<I, boolean>> {};
  }
}

export class DchMultiSelection<
  I extends string | number | symbol,
> extends DchSelectionModel<I> {
  public select(s: I) {
    this.model[s] = true;
    this.selectionChanged.fire();
    return true;
  }

  public deselect(s: I) {
    this.model[s] = false;
    this.selectionChanged.fire();
    return true;
  }
}

export class DchNoSelection<
  I extends string | number | symbol,
> extends DchSelectionModel<I> {
  declare public model: Record<I, false>;

  public select() {
    return false;
  }

  public deselect() {
    return true;
  }

  public isSelected() {
    return false;
  }
}

export class DchSingleSelection<
  I extends string | number | symbol,
> extends DchSelectionModel<I> {
  public select(s: I) {
    this.deselectAll();
    this.model[s] = true;
    this.selectionChanged.fire();
    return true;
  }

  public deselect(s: I) {
    if (this.model[s]) {
      throw new Error('Tried to deselect single selection');
    }
    return true;
  }
}
