/** @see {@link GtkPrintSettings.setPaperWidth}. @unstable */
enum GtkUnit {
  /** No units. */
  NONE,
  /** Dimension in points. */
  POINTS,
  /** Dimension in inches. */
  INCH,
  /** Dimension in millimiters. */
  MM,
}

export default GtkUnit;
