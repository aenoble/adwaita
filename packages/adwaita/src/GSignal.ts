export type GSignalCallback<AT extends unknown[]> = (
  ...args: AT
) => void;

/**
 * A class vaguely implementing Vala (GLib) signals. Not compatible with
 * Node.js's `EventEmitter`.
 *
 * @typeParam AT The argument types for callbacks as a tuple, e.g.
 * `<[string, number]>`, or with named function parameters,
 * `<[x: string, y: number]>`. These ought to be documented in the
 * property description.
 */
class GSignal<AT extends unknown[]> {
  /**
   * This constructor is meant to be used by class implementations,
   * typically in their instance init function.
   */
  public constructor() {
    this.callbacks = [];
  }

  private callbacks: GSignalCallback<AT>[];

  /**
   * Emits a signal.
   *
   * Signal emission is done synchronously. The method will only
   * return after all handlers are called or signal emission was
   * stopped.
   */
  public emit(...args: AT) {
    for (let i = 0; i < this.callbacks.length && !this.stop; i++) {
      this.callbacks[i](...args);
    }

    this.stop = false;
  }

  /**
   * @alias
   * @see {@link GSignal.emit} */
  public fire(...args: AT) {
    this.emit(...args);
  }

  /**
   * Stops a signal's current emission.
   *
   * Prints a warning if used on a signal which isn't being emitted.
   */
  public stopEmission() {
    this.stop = true;
  }
  private stop = false;

  /** Callers: copy objects in the callback. */
  public connect(callback: GSignalCallback<AT>) {
    this.callbacks.push(callback);
  }
}

export default GSignal;
