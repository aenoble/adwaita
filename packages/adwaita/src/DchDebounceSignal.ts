import GSignal from './GSignal.ts';

/**
 * {@link GSignal} with a timer for debounce.
 *
 * @typeParam AT See {@link GSignal}\<AT\>.
 *
 * @see {@link GSignal}
 */
class DchDebounceSignal<AT extends unknown[]> extends GSignal<AT> {
  /**
   * @param ms Milliseconds between successful emissions
   */
  constructor(private ms: number) {
    super();
  }

  private debounce = 0;

  emit(...args: AT) {
    const date = Date.now();
    if (this.debounce + 10 >= date) {
      throw new Error('Debounce violation');
    }
    this.debounce = date;
    super.emit(...args);
  }
}

export default DchDebounceSignal;
