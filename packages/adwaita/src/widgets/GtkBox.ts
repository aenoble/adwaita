import GtkOrientable, { GtkOrientation } from '../GtkOrientable.ts';
import GtkBaselinePosition from '../GtkBaselinePosition.ts';
import GtkWidget from '../GtkWidget.ts';
import GtkBuildable from '../GtkBuildable.ts';

/**
 * The {@link GtkBox} widget arranges child widgets into a single row or
 * column.
 *
 * Whether it is a row or column depends on the value of its
 * {@link GtkOrientable.orientation} property. Within the other
 * dimension, all children are allocated the same size. Of course, the
 * {@link GtkWidget.halign} and {@link GtkWidget.valign} properties can
 * be used on the children to influence their allocation.
 *
 * Use repeated calls to {@link GtkBox.append} to pack widgets into the
 * box from start to end. Use {@link GtkBox.remove} to remove widgets
 * from the box.{@link GtkBox.insertChildAfter} can be used to add a
 * child at a particular position.
 *
 * @todo Implement GtkAccessible & GtkConstraintTarget.
 *
 * @see {@link GtkBox.homogenous}
 * @see {@link GtkBox.spacing}
 * @see {@link GtkBox.reorderChildAfter}
 */
class GtkBox extends GtkWidget implements GtkOrientable, GtkBuildable {
  public name = 'GtkBox';

  /**
   * The position of the baseline aligned widgets if the extra space is
   * available.
   */
  public baselinePosition: GtkBaselinePosition =
    GtkBaselinePosition.CENTER;

  /** Whether the children should all be the same size. */
  public homogenous = false;

  public root: void = void 0;

  /**
   * Creates a new {@link GtkBox}.
   * @param orientation the box's orientation
   * @param spacing the number of pixels to place by default between
   * children
   */
  public constructor(
    public orientation: GtkOrientation,
    public spacing: number,
  ) {
    super();
  }

  private children: GtkWidget[] = [];

  /**
   * Adds `child` as the first child to the box.
   * @param child the {@link GtkWidget} to prepend
   */
  public prepend(child: GtkWidget) {
    this.children.unshift(child);
  }

  /**
   * Adds `child` as the last child to the box.
   * @param child the {@link GtkWidget} to append
   */
  public append(child: GtkWidget) {
    this.children.push(child);
  }

  /**
   * Removes a child widget from the box.
   *
   * The child must have been added before with {@link GtkBox.append},
   * {@link GtkBox.prepend}, or {@link GtkBox.insertChildAfter}.
   *
   * @param child the child to remove
   */
  public remove(child: GtkWidget) {
    const index = this.children.indexOf(child);
    if (index == -1) throw new Error('no such child');
    this.children.splice(index, 1);
  }

  /**
   * Inserts `child` in the position after `sibling` in the list of
   * children.
   *
   * If `sibling` is `null`, insert `child` at the first position.
   *
   * @param child the widget to insert
   * @param sibling the sibling after which to insert `child`
   */
  public insertChildAfter(child: GtkWidget, sibling: GtkWidget) {
    const index = this.children.indexOf(sibling) + 1;
    this.children.splice(index, 0, child);
  }

  /**
   * Moves `child` to the position after `sibling` in the list of
   * children.
   *
   * If `sibling` is null, move `child` to the first position.
   *
   * @param child the widget to move, must be a child of the box
   * @param sibling the sibling to move `child` after
   */
  public reorderChildAfter(child: GtkWidget, sibling: GtkWidget) {
    this.remove(child);
    this.insertChildAfter(child, sibling);
  }

  public realize() {
    const node = document.createElement('div');
    node.style.display = 'flex';
    node.style.gap = this.spacing.toString() + 'px';
    this.children.forEach((x) => {
      const xr = x.realize();
      xr.style.flex = this.homogenous ? '1 1 0' : '1 1 auto';
      xr.style.width = '100%';
      node.appendChild(xr);
    });
    return node;
  }
}

export default GtkBox;
