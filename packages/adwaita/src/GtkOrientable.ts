/**
 * The {@link GtkOrientable} interface is implemented by all widgets
 * that can be oriented horizontally or vertically.
 *
 * {@link GtkOrientable} is more flexible in that it allows the
 * orientation to be changed at runtime, allowing the widgets to "flip".
 */
interface GtkOrientable {
  /** The orientation of the orientable. */
  orientation: GtkOrientation;
}

/**
 * Represents the orientation of widgets and other objects.
 *
 * Typical examples are {@link GtkBox} or {@link GtkGesturePan}.
 *
 * @enum
 */
export enum GtkOrientation {
  /** The element is in vertical orientation. */
  HORIZONTAL,
  /** The element is in horizontal orientation. */
  VERTICAL,
}

export default GtkOrientable;
