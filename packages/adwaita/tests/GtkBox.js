import GtkBox from '../src/widgets/GtkBox.ts';
import {
  assert,
  assertThrows,
  fail,
} from 'https://deno.land/std@0.148.0/testing/asserts.ts';

function arrayEquality(a, b) {
  if (a.length != b.length) return false;
  let equal = true;
  for (let i = 0; i < a.length && equal; i++) {
    equal = a[i] == b[i];
  }
  return equal;
}

// At its core, a GtkBox simply reorganizes an array.
Deno.test('GtkBox', () => {
  const box = new GtkBox();
  box.append(1);
  box.append(2);
  box.append(3);

  box.insertChildAfter(7, 2);
  assert(arrayEquality(box.children, [1, 2, 7, 3]));

  box.reorderChildAfter(7, 3);
  assert(arrayEquality(box.children, [1, 2, 3, 7]));

  assertThrows(() => box.remove(8), Error, 'no such child');

  // TODO: Test realize.
  try {
    box.realize();
  } catch (e) {
    console.error('`realize` has no DOM to render to.');
  }
});
