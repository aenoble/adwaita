/** @file @todo DchMultiSelection tests */

import {
  DchMultiSelection,
  DchNoSelection,
  DchSingleSelection,
} from '../src/DchSelection.ts';
import {
  assert,
  assertThrows,
} from 'https://deno.land/std@0.148.0/testing/asserts.ts';

Deno.test('DchMultiSelection', () => {});

Deno.test('DchNoSelection', () => {
  const sel = new DchNoSelection({ abc: false, xyz: false });
  assert(sel.deselect('abc'), 'deselect failed');
  assert(!sel.select('xyz'), 'select succeeded');
  assert(sel.getSelection().length == 0, 'values were selected');
});

Deno.test('DchSingleSelection', () => {
  const sel = new DchSingleSelection({ abc: false, xyz: false });
  sel.select('xyz');
  const gs = sel.getSelection();
  assert(gs.length == 1, 'selection length is not 1');
  assert(gs[0] == 'xyz', 'selected item was not `xyz`');

  assert(sel.deselect('abc'));

  assertThrows(
    () => sel.deselect('xyz'),
    Error,
    undefined,
    'succeeded deselecting single selection',
  );
});
