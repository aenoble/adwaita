import {
  assertEquals,
  fail,
} from 'https://deno.land/std@0.148.0/testing/asserts.ts';

// If this fails, there needs to be `.js` on the end of an import
// somewhere.
Deno.test('Import resolution', async () => {
  assertEquals(
    (
      await Deno.permissions.query({
        name: 'read',
        path: new URL('../src', import.meta.url),
      })
    ).state,
    'granted',
    'This test requires read access to the src directory.',
  );

  import('../src/index.ts').catch((e) =>
    fail(
      'Could not resolve all import paths. You need to add `.ts` to' +
        'an import somewhere. Full error:\n\n' +
        e.message,
    )
  );
});
