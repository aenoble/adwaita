import GSignal from '../src/GSignal.ts';
import DchDebounceSignal from '../src/DchDebounceSignal.ts';

import {
  assertThrows,
  unreachable,
} from 'https://deno.land/std@0.148.0/testing/asserts.ts';

const test = (s) => {
  s.connect((s) => s.stopEmission());
  s.connect(() => unreachable('.stopEmission() failed.'));
  s.fire(s);
};

Deno.test('GSignal', () => test(new GSignal()));
Deno.test('DchDebounceSignal', () => {
  const s = new DchDebounceSignal(10);
  test(s);
  assertThrows(() => s.fire(s), Error, 'Debounce violation');
});
